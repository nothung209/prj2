import express from 'express';
import mongoose  from 'mongoose';
import dotenv from 'dotenv'
import productRouter from './routers/product.route.js';
import userRouter from './routers/user.route.js';


dotenv.config()
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
mongoose.connect('mongodb://localhost/juno', {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useCreateIndex: true,
})



app.use('/api/users', userRouter)

app.use('/api/products',productRouter)

app.get('/',(req,res) => {
    res.send('server is ready');
})

app.use(function (err, req, res, next) {
    res.status(500).send({ message: err.message });
    next()
})

app.listen(5000,() => {
    console.log('Serve at http://localhost:5000');
});